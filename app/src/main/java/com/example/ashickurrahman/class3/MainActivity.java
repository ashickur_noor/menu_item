package com.example.ashickurrahman.class3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnEdit;
    Button btnShow;
    Button btnDelete;
    Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEdit= findViewById(R.id.edit);
        btnShow= findViewById(R.id.show);
        btnDelete= findViewById(R.id.delete);
        btnUpdate= findViewById(R.id.update);

        btnDelete.setOnClickListener(this);
        btnEdit.setOnClickListener(this);
        btnShow.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        btnUpdate.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(MainActivity.this,btnUpdate);
                popupMenu.getMenuInflater().inflate(R.menu.my_menu,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        return false;
                    }
                });
                popupMenu.show();
            }
        });

    }

    @Override
    public void onClick(View view) {
    // main view click, fetch here

        switch (view.getId()){
            case R.id.edit:
                Toast.makeText( this, "Edit",Toast.LENGTH_LONG).show();
                break;
//            case R.id.update:
//                Toast.makeText( getApplicationContext(), "Update",Toast.LENGTH_LONG).show();
//                break;
            case R.id.delete:
                Toast.makeText( MainActivity.this, "Delete",Toast.LENGTH_LONG).show();
                break;
            case R.id.show:
                Toast.makeText( MainActivity.this, "Show",Toast.LENGTH_LONG).show();
                break;

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_menu,menu);
        //একটা ইন্টারফেসে আরেকটা ইন্টারফেস ব্যবহার করতে হলে linflate ব্যবহার করা হয়।
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //মেনুগুলাতে ট্যাপ করলে তা কাজ করানোর জন্য

        switch (item.getItemId()){
            case R.id.medit:
                Toast.makeText( MainActivity.this, "Menu edit",Toast.LENGTH_LONG).show();
                break;
            case R.id.mshow:
                Toast.makeText( MainActivity.this, "Menu Show",Toast.LENGTH_LONG).show();
                break;
            case R.id.mupdate:
                Toast.makeText( MainActivity.this, "Menu Update",Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
